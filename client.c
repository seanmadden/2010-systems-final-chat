#include <sys/types.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <stdio.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <errno.h>
#include "clientEssential.h"



int main (int argc, char *argv[])
{
	/****************************************
	 *argv[0] = a.out
	 *argv[1] = ip address
	 *argv[2] = port
	 ****************************************/

	int sockfd, len, result, x, choice;
	struct sockaddr_in address;
	msg receive, send;
	char *onlineUsers[100];
	int state = 0; // 0 = not in a chat, 1 = in a chat, 2 = sending a file

	for (x = 0; x < 100; x++)
		onlineUsers[x] = NULL;

	x = 1;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd == -1)
	{
		printf("Socket Failed\n");
		return 0;
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr(argv[1]);
	address.sin_port = htons(atoi((char*)argv[2]));
	len = sizeof(address);

	if (connect(sockfd, (struct sockaddr *)&address, len) == -1 && errno != 115)
	{
		printf("%s\n",strerror(errno));
		return 0;
	}

	receive.flag = 0;
	while (receive.flag != 999)
	{
		
		if (read(sockfd, &receive, sizeof(receive)) > 0)
		{
//			printf("I read: %i - %s", receive.flag, receive.mes);
			switch (receive.flag)
			{
				case 56://get choice from user
				{
					printf("%s", receive.mes);
					fflush(stdout);
					scanf("%i", &choice);

					while (choice != 1 && choice != 2)
					{
						printf("\nWrong Choice try again: ");
						scanf("%i", &choice);
					}

					if (choice == 1)
					{
						send.flag = 100;//request username
					}

					else if (choice == 2)
					{
						send.flag = 200;//request add user
					}
					break;
				}

				case 101://get username from user
				{
					printf("%s", receive.mes);
					scanf("%s", send.from);
					send.flag = 102;//check username
					break;
				}

				case 105://get password from user
				{
					
					printf("%s", receive.mes);
					strcpy(send.mes, makePass(getpass("")));
					send.flag = 106;//send check password
					break;
				}

				case 201://get desired username from user
				{
					printf("%s", receive.mes);
					scanf("%s", send.from);
					send.flag = 202;//check username for existance and add

					break;
				}

				case 203://get password for new user
				{
					printf("%s", receive.mes);
					strcpy(send.mes, makePass(getpass("")));
					send.flag = 204;
					break;
				}

				case 400://logged in no need to send anything back, just wait for input
				{
					printf("%s\n", receive.mes); //TODO: add a menu
					//block(sockfd);
					//send.flag = 700;
					send.flag = -1;
					receive.flag = 999; // exit the loop

					break;
				}


					

				case 999://end chat
				{
					printf("%s", receive.mes);
					return 0;
					break;
				}

				default:
				{
					printf("receive.flag = %i\n", receive.flag);
					printf("\ninside default\n");
					return 0;
				}
			}//end switch
			if (send.flag != -1)
				write(sockfd, &send, sizeof(send));

			send.flag = -1;
		}//end if
	}//end while



	// A BRAND NEW WHILE LOOP THAT WILL BE NONBLOCKING
	

	//send.flag = 700;
	//sprintf(send.mes, "0");
	//write(sockfd, &send, sizeof(send));

	noblock(0); 		// non-blocking stdin
	noblock(sockfd);	// non-blocking socket
	choice = 8756;


	while (1)
	{
		if (read(sockfd, &receive, sizeof(receive)) > 0)
		{
			switch (receive.flag)
			{
				case 601:
				{
					if (state == 0)
					{
						state = 1;
						strcpy(send.to, receive.from);
						putInColor(receive.mes, GREEN);
					}

					else if (state == 1)
					{
						if (strcmp(receive.from, send.to) != 0)
						{
							char *old = malloc(sizeof(send.to));
							strcpy(old, send.to);

							strcpy(send.to, receive.from);
							send.flag = 710;
							//strcpy(send.mes, "\nUser is in a chat");
							write(sockfd, &send, sizeof(send));
							
							strcpy(send.to, old);
							//state = 0;
							send.flag = -1;
							break;
						}
						putInColor(receive.mes, GREEN);
					}

					/*printf("in 601");
					printf("in chat with: %s", receive.from);
					fflush(stdout);
					*/
//					printf("%s\n", receive.mes);
					
					send.flag = -1;

					break;
				}
				case 603:
				{
					int f, wrote; // file
					f = open("received", O_CREAT | O_APPEND | O_WRONLY, 0644);
					wrote = write(f, receive.mes, strlen(receive.mes));
					printf("\nFile being received as: received\n");

					close(f);
					state = 0;
					send.flag = -1;
				}
				case 701:
				{

					int x = 0;
					onlineUsers[atoi(receive.mes)] = malloc(sizeof(receive.to));
					strcpy(onlineUsers[atoi(receive.mes)], receive.to);
					x = atoi(receive.mes);
					x++;
					send.flag = 700;
					sprintf(send.mes, "%i", x);

					break;
				}

				case 705:
				{
					if (state == 1)
					{
						printf("\nOnline Users:\n");
						showOnlineUsers(onlineUsers);
						printf("\nEnter user number to chat with: ");

						block(0);

						scanf("%i", &choice);
						printf("\nMessage: ");
						getMessage(send.mes, 0);

						noblock(0);

						send.flag = 600;
						strcpy(send.to, onlineUsers[choice - 1]);
						strcpy(receive.from, send.to);
						noblock(sockfd);
						break;
					} else if (state == 2) {

						char fname[50];
						char *fptr = fname;
						
						printf("\nOnline Users:\n");
						showOnlineUsers(onlineUsers);
						printf("\nEnter user number to send file to: ");

						block(0);

						scanf("%i", &choice);
						printf("\nfile to send: ");
						getMessage(fptr, 0);

						noblock(0);

						strcpy(send.to, onlineUsers[choice - 1]);
						strcpy(receive.from, send.to);

						sendfile(sockfd, fptr, send.to);
						state = 0;

						noblock(sockfd);
						send.flag = -1;
						
					}
				}
				
				case 720:
				{
					if (state == 1)
					{
						printf("User is already in chat.");
						send.flag = 700;
						state = 0;
					}
				}
			}

			if (send.flag != -1)
				write(sockfd, &send, sizeof(send));
		}
		else // I didn't get any incoming data
		{
			if (state == 0) // not in a chat
			{

				if ((int) choice != -1)
					drawMenu();
				//scanf("%i", &choice);
				fflush(stdout);
				choice = getMessage2(1);
				//printf("choice is: %i\n", (int)choice);
				fflush(stdout);
				switch (choice)
				{
					case '1': // start a new chat
					{
						send.flag = 700;
						strcpy(send.mes, "0");
						state = 1;
						block(sockfd);

						write(sockfd, &send, sizeof(send));
						noblock(sockfd);
						break;
					}

					case '2': // send a file
					{
						send.flag = 700;
						strcpy(send.mes, "0");
						state = 2;
						block(sockfd);

						write(sockfd, &send, sizeof(send));
						noblock(sockfd);
						//TODO send file
						break;
					}

					case '0': // logging off...
					{
						return 0;
						break;
					}
				}
			} else { // you are in a chat
				send.flag = -1;
				//printf("in else, in chat\n");
				fflush(stdout);
				strcpy(send.mes, " ");
				//printf("after strcpy\n");
				//printf("chatting with %s", receive.from);
				getMessage(send.mes, 1);
				//printf("this shit is: %i\n", (int)send.mes);
				fflush(stdout);
				if (strcmp(send.mes, "exit") == 0)
				{
					state = 0;
					//send.flag = 700;
					//write(sockfd, &send, sizeof(send));
				}
				else if (*send.mes)
				{
					send.flag = 600;
					strcpy(send.to, receive.from);
					//printf("in if after other strcpy\n", send.mes);
					//fflush(stdout);
					write(sockfd, &send, sizeof(send));
				}
				
			}


		}
	}
	return 0;

}

