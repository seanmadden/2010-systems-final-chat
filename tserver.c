#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <fcntl.h>

typedef struct 
{
	unsigned char msgID;
	unsigned char flags;
} msg;



int main(int argc, char *argv[])
{
	/**************************************************
	 *argv[0] = a.out
	 *argv[1] = ip address to bind to (not used)
	 *argv[2] = port to bind to
	 **************************************************/

	int s, s2, len, x;
	struct sockaddr_in sock, far;
	char buf[100];
	char xy[50];
	struct tm *now;
	time_t pres;
	msg msg1;
	int flags;
	signal(SIGCHLD, SIG_IGN);


	strcpy(xy, "");

	s = socket(PF_INET, SOCK_STREAM, 0);
	flags = fcntl(s, F_GETFL);
	flags |= O_NONBLOCK;
	fcntl(s, F_SETFL, flags);

	flags = fcntl(0, F_GETFL);
	flags |= O_NONBLOCK;
	fcntl(0, F_SETFL, flags);
	
	sock.sin_port = htons(atoi(argv[2]));
	//sock.sin_port = htons(6982);
	sock.sin_addr.s_addr = htonl(INADDR_ANY);
	sock.sin_family = AF_INET;
	len = sizeof(sock);
	x = bind(s, (struct sockaddr *)&sock, len);
	if (x == -1)
	{
		printf("bind error: %s\n", strerror(errno));
		return 0;
	}

	len = sizeof(struct sockaddr_in);
	
	while (1)
	{
		//printf("Waiting for input...\n");
		//fflush(stdout);
		listen(s, 5);

		scanf("%s", xy);
		if(strcmp(xy, "a") >= 0)
		{
			printf("a;lskhfasf");
			return 0;
		}
		fflush(stdout);

		s2 = accept(s, (struct sockaddr *)&far, &len);
		if (!fork())
		{ // this is the child, it should handle s2
			x = read(s2, &msg1, sizeof(msg1)); // read the request into the buffer
			//printf("x is %i, buf is %s\n", x, buf);
			//printf("I'm inside the child, I should do something\n");
			//printf("msgid is %i, flags are %i", msg1.msgID, msg1.flags);
			pres = time(0);
			now = localtime(&pres);

			switch (msg1.msgID)
			{
				case 1:
					if (msg1.flags == 1)
						strftime(buf, 100, "The month number is %m", now);
					else if (msg1.flags == 2)
						strftime(buf, 100, "The month name is %B", now);
					else
						strcpy(buf, "Wrong parameters");
					break;
				case 2:
					if (msg1.flags == 1)
						strftime(buf, 100, "The day number is %d", now);
					else if (msg1.flags == 2)
						strftime(buf, 100, "The day is %a", now);
					else 
						strcpy(buf, "Wrong parameters");
					break;
				case 3:
					if (msg1.flags == 1)
						strftime(buf, 100, "The year is %Y", now);
					else if (msg1.flags == 2)
						strftime(buf, 100, "The two digit year is %y", now);
					else
						strcpy(buf, "Wrong parameters");
					break;
				case 4:
					if (msg1.flags == 1)
						strftime(buf, 100, "The hour is %I", now);
					else if (msg1.flags == 2)
						strftime(buf, 100, "The hour is %H", now);
					else
						strcpy(buf, "Wrong parameters");
					break;
				case 5:
					if (msg1.flags == 1)
						strftime(buf, 100, "The day of the month is %d", now);
					else
						strcpy(buf, "Wrong parameters");
					break;
				default:
					strcpy(buf, "Wrong parameters");
			}
			write(s2, buf, 100);


			return 0;
		}
	}

	




	return 0;

}
