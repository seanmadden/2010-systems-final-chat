#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <fcntl.h>

void block(int fd);
void noblock(int fd);

int main()
{
	char temp;
	char mes[1000];
	int i = 0;
	int val;
	fd_set rfds;
	struct timeval tv;

	noblock(0);

	FD_ZERO(&rfds);
	FD_SET(0, &rfds);

	tv.tv_sec = 5;
	tv.tv_usec = 0;

	//if (select(1, &rfds, NULL, NULL, &tv) > 0);
		temp = fgetc(stdin);

	while ((int)temp != -1 && (int)temp != 10)
	{
		printf("I read: %c\n", temp);
		mes[i++] = temp;
		temp = fgetc(stdin);
	}
	mes[i] = '\0';
	if (*mes)
		printf("mes: %i\n", (int)*mes);
	return 0;
}

void block(int fd)
{
	int flags;
	flags = fcntl(fd, F_GETFL);			//
	flags &= !O_NONBLOCK;				//make stdin nonblocking
	fcntl(fd, F_SETFL, flags);			//
}

void noblock(int fd)
{
	int flags;
	flags = fcntl(fd, F_GETFL);			//
	flags |= O_NONBLOCK;				//make stdin nonblocking
	fcntl(fd, F_SETFL, flags);			//
}
	
