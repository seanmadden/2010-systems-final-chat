#include "color.h"


void textcolor(int attr, int fg, int bg) {

	// Function to change the foreground color of the text

	char command[13];

	/* Command is the control command to the terminal */
	sprintf(command, "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
	printf("%s", command);
}

