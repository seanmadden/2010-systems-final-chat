#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

void readline(char buf2[50], FILE *fil);
void clearbuf(char buf[50]);
int copy (int s, FILE *fil);

int copy (int s, FILE *fil)
{
	char *sname;
	int copies, success, i, len, j;
	char buf[50], buf2[50], singlec, *cptr;
	FILE *fil;
	FILE *wfil;

	else
	{
		sname = argv[1];
		if (atoi(argv[2]) < 1)
		{
			printf("ERROR: Invalid number of copies to make\n");
			return 0;
		}
		copies = atoi(argv[2]);
		
		fil = fopen(sname, "r");
		if (!fil)
		{
			printf("ERROR: Source file does not exist\n");
			return 0;
		}
		
	}

	//set the files to be empty
	for (i = 1; i < copies + 1; i++)
	{
		sprintf(buf2, "%s%d", sname, i);

		wfil = fopen(buf2, "w");
		success = fclose(wfil);
	}
	
	cptr = buf; 

	readline(cptr, fil);
	while (!feof(fil))
	{
		for (i = 1; i < copies + 1; i++)
		{
			sprintf(buf2, "%s%d", sname, i);

			wfil = fopen(buf2, "a+");

			len = strlen(cptr);

			success = fwrite(cptr, sizeof(char), len, wfil);
			if (success)
			{
				success = fclose(wfil);
				//success = fread(buf, sizeof(char), sizeof(buf), fil);
			}
			else
			{
				printf("ERROR: Something went wrong\n");
				success = fclose(wfil);
				return 0;
			}
		}
		//success = fread(buf2, sizeof(char), sizeof(buf), fil);
		clearbuf(cptr);
		readline(cptr, fil);
	}


	return 0;
}

void readline(char buf[50], FILE *fil)
{
	int j = 0;
	char singlec;
	singlec = fgetc(fil);
	while((singlec != '\n' && singlec != EOF) || j >= 49)
	{
		buf[j++] = singlec;
		singlec = fgetc(fil);
	}
	if (singlec == '\n')
		buf[j++] = '\n';
	else
		buf[j++] = EOF;
	
	

}

void clearbuf(char buf[50])
{
	int i;
	for (i = 0; i < 50; i++)
		buf[i] = (char) NULL;
		

}

