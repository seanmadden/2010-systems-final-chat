#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <fcntl.h>
#include "serverEssential.h"



int main(int argc, char *argv[])
{
	/**************************************************
	 *argv[0] = a.out
	 *argv[1] = ip address to bind to (not used)
	 *argv[2] = port to bind to
	***************************************************/

	//int s[100], listenSock; //array of 100 sockets and pointers to those sockets
	int listenSock;
	//user users[100]; // runs parallel to the socket array
	int connCount = 0; //connected count
	struct sockaddr_in sock, far; // structure for our local and far sockets
	int i, x; // lcv
	int len, nextSocket; // socket things
	msg send, receive; // structures that will carry data
	char *onlineUsers[100]; // array of online users


	for (i = 0; i < 100; i++)
	{
		noblock(s[i]);
		s[i] = -1; // all sockets start with 0 (no connection);
	}

	listenSock = socket(PF_INET, SOCK_STREAM, 0); // initalize all of the sockets
	noblock(listenSock);

	sock.sin_port = htons(atoi(argv[2]));
	sock.sin_addr.s_addr = htonl(INADDR_ANY);
	sock.sin_family = AF_INET;
	len = sizeof(sock);


	x = bind(listenSock, (struct sockaddr *)&sock, len);
	
	if (x == -1) // bind the socket on the port
	{
		printf("bind error: %i: %s\n", errno, strerror(errno));
		return 0;
	}

	len = sizeof(struct sockaddr_in);
	block(listenSock);
	

	while (1)
	{
		listen(listenSock, 5); // s[0] will always be the listening port

		nextSocket = findNextAvailable(s);
		//fflush(stdout);
		if (nextSocket == -1)
		{
			printf("fatal error\n");
			return 0;
		}

		s[nextSocket] = accept(listenSock, (struct sockaddr *)&far, &len); // accepts on next socket
		noblock(listenSock);
		//fflush(stdout);
		if (s[nextSocket] != -1) // a new client was connected, let's get them authenticated
		{
			users[nextSocket].socketNum = nextSocket;
			connCount++;
			strcpy(send.mes, "Welcome to ACM Chat\n\nAre you a:\n(1)Existing User\n(2)New User\nEnter your choice: ");
			send.flag = 56;
			write(s[nextSocket], &send, sizeof(send)); // new or existing user
		}

		for (i = 0; i < 100; i++)
		{
			noblock(s[i]);
			if (s[i] != -1) // someone is connected there
			{
				if (read(s[i], &receive, sizeof(receive)) > 0) 
				{
					//fflush(stdout);
					switch (receive.flag)
					{
						
						case 100: // I'm an existing user
						{
							strcpy(send.mes, "\nEnter your user name: ");
							send.flag = 101; // what is your name?
							break;
						}


						case 102: // here is my username
						{
							char pass[50];
							char *pass_ptr;
							pass_ptr = pass;

							// TODO: segfaults on badd user name
							pass_ptr = searchForUsername(receive.from);
							//strcpy(users[i].pass, searchForUsername(receive.from));
							if (pass_ptr/*users[i].pass*/) // they exist!
							{
								strcpy(users[i].pass, pass_ptr);
								strcpy(users[i].uname, receive.from);
								send.flag = 105; // what is your password?
								strcpy(send.mes, "\nEnter your password: ");
							} else {
								send.flag = 999;
								strcpy(send.mes, "\nUsername non existent, disconnecting you.\n");
								write(s[i], &send, sizeof(send));
								close(s[1]);
								s[i] = -1;
							}
							break;
						}

						case 106:
						{
							if (strcmp(receive.mes, users[i].pass) == 0) // they match, go on
							{
								send.flag = 400;
								strcpy(send.mes, "\nAccepted, thank you for logging on");
							} else {
								send.flag = 999;
								strcpy(send.mes, "\nInvalid password, Disconnected.\n");
								write(s[i], &send, sizeof(send));
								close(s[i]);
								s[i] = -1;
							}
							break;
						}

						case 200: // I want to make a new account
						{
							send.flag = 201;	
							strcpy(send.mes, "\nEnter your desired user name: ");
							break;
						}

						case 202: // is the name I want available?
						{
							if (!searchForUsername(receive.from))
							{
								strcpy(users[i].uname, receive.from);
								send.flag = 203;
								strcpy(send.mes, "\nUsername avaiable, enter desired password: ");
							} else {
								send.flag = 999;
								strcpy(send.mes, "\nUsername in use, disconnecting you.\n");
								write(s[i], &send, sizeof(send));
								close(s[1]);
								s[i] = -1;
							}

							break;
						}

						case 204: // this is my new password
						{
							strcpy(users[i].pass, receive.mes);
							if (createUser(users[i].uname, users[i].pass))
							{
								send.flag = 400;
								strcpy(send.mes, "\nAccount created, you are now logged in");
							} else {
								send.flag = 999;
								strcpy(send.mes, "\nAccount could not be created. Disconnected.\n");
								write(s[i], &send, sizeof(send));
								close(s[1]);
								s[i] = -1;
							}

							break;
						}

						case 600: //message
						{
							x = getNum(receive.to);
							sprintf(send.mes, "\n%s: %s", users[i].uname, receive.mes);
							send.flag = 601;
							strcpy(send.to, users[i].uname);
							strcpy(send.from, receive.from);

							write(s[x], &send, sizeof(send));
							send.flag = -1;

							break;
						}

						case 620: // incoming file, redirect it to the proper client
						{
							x = getNum(receive.to);
							strcpy(send.mes, receive.mes);
							send.flag = 603;
							strcpy(send.to, users[i].uname);
							strcpy(send.from, receive.from);

							//printf("\ns[x] is: %i\n", s[x]);
							fflush(stdout);
							write(s[x], &send, sizeof(send));

							send.flag = -1;
							break;


						}

						case 700: // gimme user in hole receive.mes
						{

							send.flag = 701;
							if (s[atoi(receive.mes)] != -1)
							{
								sprintf(send.to, "%s", users[atoi(receive.mes)].uname);
								sprintf(send.mes, "%i", atoi(receive.mes));
							} else {
								send.flag = 705;
							}

							break;
						}
						
						case 710: //user is already in a chat
						{
							fflush(stdout);
							x = getNum(receive.to);
							send.flag = 720;
							strcpy(send.to, users[i].uname);
							strcpy(send.from, receive.from);

							write(s[x], &send, sizeof(send));
							send.flag = -1;
							break;

						}

						case 998: // log off
						{
							close(s[i]);
							s[i] = -1;
							send.flag = -1;
							break;

						}

						default:
						{
							//printf("NOTHING!\n");
						}


					}
					
					if (s[i] != -1 && send.flag != -1)
					{
						write(s[i], &send, sizeof(send));
						send.flag = -1;
					}
						
				}
			}
		}
				

		//sleep(1);
	}

	return 0;
}

