#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <crypt.h>

#define RESET           0
#define BRIGHT          1
#define DIM             2
#define UNDERLINE       3
#define BLINK           4
#define REVERSE         7
#define HIDDEN          8

#define BLACK           0
#define RED             1
#define GREEN           2
#define YELLOW          3
#define BLUE            4
#define MAGENTA         5
#define CYAN            6
#define WHITE           7

typedef struct
{
	char to[50];
	char from[50];
	char mes[1000];
	int flag;
} msg;

char *crypt(const char *key, const char *salt);
char *makePass(char *plainpass);
void textcolor(int attr, int fg, int bg);

void block(int fd);
void noblock(int fd);

void putInColor(char *text, int color);

