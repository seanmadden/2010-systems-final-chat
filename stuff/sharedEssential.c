#include "password.h"
#include "sharedEssential.h"
#include "color.h"

char *crypt(const char *key, const char *salt);

char *makePass(char *plainpass)
{
	char *salt = "02";

	return (char *)crypt(plainpass, salt);
}

void putInColor(char *text, int color)
{
	// Easier to use function to colorize text
	textcolor(BRIGHT, color, BLACK);
	printf("%s\n", text);
	textcolor(RESET, WHITE, BLACK);
	fflush(stdout);
}

void textcolor(int attr, int fg, int bg) {

	// Function to change the foreground color of the text

	char command[13];

	/* Command is the control command to the terminal */
	sprintf(command, "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
	printf("%s", command);
}

void block(int fd)
{
	int flags;
	flags = fcntl(fd, F_GETFL);			//
	flags &= !O_NONBLOCK;				//make stdin nonblocking
	fcntl(fd, F_SETFL, flags);			//
}

void noblock(int fd)
{
	int flags;
	flags = fcntl(fd, F_GETFL);			//
	flags |= O_NONBLOCK;				//make stdin nonblocking
	fcntl(fd, F_SETFL, flags);			//
}
	
