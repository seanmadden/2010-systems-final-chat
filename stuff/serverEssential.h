#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sharedEssential.h"

typedef struct
{
	char uname[50];
	char pass[30];
	int socketNum;
} user;

user users[100];
int s[100];

char *searchForUsername(char *username);

int findNextAvailable(int *s);
	// passes in the structure of sockets and find the first one that is open

int createUser(char *name, char *pass);
	// return 0 on failure, 1 on success

int listOnline(char *ou[]);
	// returns the number of online users
int getNum(char *name);
	// returns the socket num the user's on
