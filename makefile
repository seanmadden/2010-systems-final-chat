all: client server

client: client.o clientEssential.o sharedEssential.o
	gcc -g -lcrypt -lncurses -o client client.o clientEssential.o sharedEssential.o

server: server.o serverEssential.o sharedEssential.o 
	gcc -g -lcrypt -o server server.o serverEssential.o sharedEssential.o

server.o: server.c
	gcc -g -c server.c

client.o: client.c
	gcc -g -c client.c

clientEssential.o: clientEssential.c
	gcc -c clientEssential.c
	
sharedEssential.o: sharedEssential.c
	gcc -c sharedEssential.c

serverEssential.o: serverEssential.c
	gcc -c serverEssential.c
