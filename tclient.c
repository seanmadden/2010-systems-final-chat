#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>


int main (int argc, char *argv[])
{
	typedef struct
	{
		unsigned char MsgID;
		unsigned char Flags;
	} msg;

	int sockfd, len, result;
	struct sockaddr_in address;
	char buf[100];
	msg msg1;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd == -1)
	{
		printf("Socket Failed\n");
		return 0;
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr(argv[1]);
	address.sin_port = htons(atoi((char*)argv[2]));
	len = sizeof(address);

	if (connect(sockfd, (struct sockaddr *)&address, len) == -1)
	{
		printf("Connect Failed\n");
		return 0;
	}

	msg1.MsgID = atoi(argv[3]);
	msg1.Flags = atoi(argv[4]);

	write(sockfd, &msg1, 2);
	read(sockfd, buf, 100);
	printf("%s\n", buf);


}

