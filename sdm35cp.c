#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>


int main (int argc, char *argv[])
{
	char *sname, *back;
	int copies, success, i, j, numread, numwrote, first;
	char buf[50], buf2[50], *cptr; //buf handles files, buf2 handles reading the blocks
	int fil;
	int wfil;

	first = 1;

	if (argc != 3) //should be a source and a destination
	{
		printf("ERROR: Must specify a source file and the number of copies to make\n");
		return 0;
	}
	else
	{
		sname = argv[1]; 

		if (atoi(argv[2]) < 1) //check the number of copies
		{
			printf("ERROR: Invalid number of copies to make\n");
			return 0;
		}
		copies = atoi(argv[2]);

		fil = open(sname, O_RDONLY); //check the name to make sure the source file is there
		if (fil < 0)
		{
			printf("ERROR: Source file does not exist\n");
			return 0;
		}
		
	}


	numread = read(fil, buf2, 50);
	cptr = strstr(sname, ".");
	if (cptr)
	{
		j = strcspn(sname, ".");
		back = malloc(strlen(cptr));
		strcpy(back, cptr);

		sname[j] = (char) NULL;
	}


	
	while (numread > 0)
	{
		for (i = 1; i < copies + 1; i++)
		{
			if (cptr)
			{
				sprintf(buf, "%s%d%s", sname , i, back);
			}
			else
				sprintf(buf, "%s%d", sname, i);
			if (first)
				wfil = open(buf, O_WRONLY | O_APPEND | O_TRUNC | O_CREAT, 0644);
			else
				wfil = open(buf, O_WRONLY | O_APPEND);

			numwrote = write(wfil, buf2, numread);
			if (numwrote == numread)
			{
				success = close(wfil);
			}
			else
			{
				
				printf("ERROR: Something went terribly wrong\n");
				success = close(wfil);
				return 0;
			}
		}
		//printf("I read %d and I wrote %d\n", numread, numwrote);
		first = 0;
		numread = read(fil, buf2, 50);
	}

	if (numread < 0)
		printf("ERROR: Something went extra terribly wrong\n");

	if (cptr)
		free(back);
	return 0;
}
