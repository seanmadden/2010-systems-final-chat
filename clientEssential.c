#include "clientEssential.h"


void drawMenu()
{
	int i;
	system("clear");
	printf("== Chat ==\n");
	printf("1. Start a chat\n");
	printf("2. Send a file\n");
	printf("0. Logout\n");
	printf("== Chat ==\n");
	printf("Enter your option: ");

}

void showOnlineUsers(char *online[])
{
	int i;
	for (i = 0; i < 100; i++)
	{
		if (online[i] == NULL)
			break;
		else
			printf("%i. %s\n", i + 1, online[i]);
	}
}	

void getMessage(char *mes, int wait)
{
	char temp;
	int i = 0;
	int val;
	fd_set rfds;
	struct timeval tv;

	//noblock(0);

	FD_ZERO(&rfds);
	FD_SET(0, &rfds);

	tv.tv_sec = wait;
	tv.tv_usec = 0;

	if (wait == 0)
		fgets(mes, 1000, stdin);
	else if (select(1, &rfds, NULL, NULL, &tv) > 0);
		temp = fgetc(stdin);

	while ((int)temp != -1 && (int)temp != 10)
	{
		mes[i++] = temp;
		temp = fgetc(stdin);
	}
	mes[i] = '\0';

}

char getMessage2(int wait)
{
	char temp;
	int val;
	fd_set rfds;
	struct timeval tv;

	//noblock(0);

	FD_ZERO(&rfds);
	FD_SET(0, &rfds);

	tv.tv_sec = wait;
	tv.tv_usec = 0;

	
	if (select(1, &rfds, NULL, NULL, &tv) > 0)
		temp = fgetc(stdin);
	else
		return -1;

	return temp;

}

int sendfile(int sockfd, char *fname, char *destination)
{
	
	int success, i, j, numread, numwrote, first;
	char buf[50], buf2[50], *cptr; //buf handles files, buf2 handles reading the blocks
	int fil;
	msg send;

	first = 1;



	fil = open(fname,  O_RDONLY); //check the name to make sure the source file is there
	if (fil < 0)
	{
		printf("ERROR: Source file does not exist\n");
		return 0;
	}
		


	numread = read(fil, buf, 50);
	strcpy(send.mes, buf);


	strcpy(send.to, destination);
	//send.mes[strlen(buf) + 1] = '\0';
	
	
	while (numread > 0)
	{
		send.flag = 620;
		block(sockfd);
		numwrote = write(sockfd, &send, sizeof(send));
		noblock(sockfd);
		numread = read(fil, buf, 50);
		if (numread < 50)
		{
			buf[numread] = '\0';
		}
		strcpy(send.mes, buf);
	}
	//printf("I read %d and I wrote %d\n", numread, numwrote);

	if (numread < 0)
		printf("ERROR: Something went extra terribly wrong\n");

}
